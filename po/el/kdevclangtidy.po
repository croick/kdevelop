# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdevelop package.
#
# Stelios <sstavra@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdevelop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2019-08-26 12:21+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: analyzer.cpp:29
#, kde-format
msgid "Clang-Tidy"
msgstr "Clang-Tidy"

#: config/checklistfilterproxysearchline.cpp:23
#, fuzzy, kde-format
#| msgid "Search"
msgctxt "@info:placeholder"
msgid "Search..."
msgstr "Αναζήτηση"

#: config/checklistmodel.cpp:65
#, fuzzy, kde-format
#| msgid "All checks"
msgctxt "@item"
msgid "All checks"
msgstr "όλοι οι έλεγχοι"

#: config/checksetmanagewidget.cpp:89
#, kde-format
msgctxt "@title:window"
msgid "Enter Name of New Check Set"
msgstr ""

#: config/checksetmanagewidget.cpp:95
#, kde-format
msgctxt "@label:textbox"
msgid "Name:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: config/checksetmanagewidget.ui:31
#, fuzzy, kde-format
#| msgid "Checks"
msgctxt "@label:listbox"
msgid "Check set:"
msgstr "Έλεγχοι"

#. i18n: ectx: property (toolTip), widget (QPushButton, addCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:48
#, fuzzy, kde-format
#| msgid "All checks"
msgctxt "@info:tooltip"
msgid "Add check set"
msgstr "όλοι οι έλεγχοι"

#. i18n: ectx: property (toolTip), widget (QPushButton, cloneCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:58
#, fuzzy, kde-format
#| msgid "Checks"
msgctxt "@info:tooltip"
msgid "Clone check set"
msgstr "Έλεγχοι"

#. i18n: ectx: property (toolTip), widget (QPushButton, removeCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:68
#, fuzzy, kde-format
#| msgid "All checks"
msgctxt "@info:tooltip"
msgid "Remove check set"
msgstr "όλοι οι έλεγχοι"

#. i18n: ectx: property (toolTip), widget (QPushButton, setAsDefaultCheckSetSelectionButton)
#: config/checksetmanagewidget.ui:78
#, kde-format
msgctxt "@info:tooltip"
msgid "Set as default"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QPushButton, editCheckSetSelectionNameButton)
#: config/checksetmanagewidget.ui:88
#, kde-format
msgctxt "@info:tooltip"
msgid "Edit name of check set"
msgstr ""

#: config/checksetselectioncombobox.cpp:26
#, kde-format
msgctxt "@item:inlistbox"
msgid "Custom"
msgstr ""

#: config/checksetselectioncombobox.cpp:30
#, kde-format
msgctxt "@item:inlistbox"
msgid "Use default (currently: %1)"
msgstr ""

#: config/checksetselectionlistmodel.cpp:64
#, kde-format
msgctxt "@item:inlistbox"
msgid "%1 (default selection)"
msgstr ""

#: config/clangtidypreferences.cpp:62 config/clangtidyprojectconfigpage.cpp:55
#, fuzzy, kde-format
#| msgid "Clang-Tidy"
msgctxt "@title:tab"
msgid "Clang-Tidy"
msgstr "Clang-Tidy"

#: config/clangtidypreferences.cpp:67
#, fuzzy, kde-format
#| msgid "Configure Clang-Tidy Settings"
msgctxt "@title:tab"
msgid "Configure Clang-Tidy Settings"
msgstr "Διαμόρφωση ρυθμίσεων Clang-Tidy"

#. i18n: ectx: property (title), widget (QGroupBox, pathsGroupBox)
#: config/clangtidypreferences.ui:29
#, fuzzy, kde-format
#| msgid "Paths"
msgctxt "@title:group"
msgid "Paths"
msgstr "Διαδρομές"

#. i18n: ectx: property (text), widget (QLabel, clangtidyLabel)
#: config/clangtidypreferences.ui:37
#, fuzzy, kde-format
#| msgid "Clang-&tidy executable:"
msgctxt "@label:chooser"
msgid "Clang-&Tidy executable:"
msgstr "Εκτελέσιμο Clang-&tidy:"

#. i18n: ectx: property (toolTip), widget (KUrlRequester, kcfg_clangtidyPath)
#: config/clangtidypreferences.ui:54
#, fuzzy, kde-format
#| msgid "The full path to the clang-tidy executable"
msgctxt "@info:tooltip"
msgid "The full path to the Clang-Tidy executable"
msgstr "Η πλήρης διαδρομή στο εκτελέσιμο clang-tidy"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_parallelJobsEnabled)
#: config/clangtidypreferences.ui:72
#, fuzzy, kde-format
#| msgid "Run analysis jobs in parallel"
msgctxt "@option:check"
msgid "Run analysis jobs in parallel"
msgstr "Παράλληλη εκτέλεση εργασιών ανάλυσης"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_parallelJobsAutoCount)
#: config/clangtidypreferences.ui:81
#, fuzzy, kde-format
#| msgid "Use all CPU cores"
msgctxt "@option:check"
msgid "Use all CPU cores"
msgstr "Χρήση όλων των CPU πυρήνων"

#. i18n: ectx: property (text), widget (QLabel, parallelJobsFixedCountLabel)
#: config/clangtidypreferences.ui:101
#, fuzzy, kde-format
#| msgid "Maximum number of threads:"
msgctxt "@label:spinbox"
msgid "Maximum number of threads:"
msgstr "Μέγιστο πλήθος νημάτων:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_useConfigFile)
#: config/clangtidyprojectconfigpage.ui:29
#, fuzzy, kde-format
#| msgid "&Use .clang-tidy file(s)"
msgctxt "@option:check"
msgid "&Use .clang-tidy file(s)"
msgstr "&Χρήση αρχείων .clang-tidy"

#. i18n: ectx: attribute (title), widget (QWidget, checksTab)
#: config/clangtidyprojectconfigpage.ui:43
#, fuzzy, kde-format
#| msgid "Checks"
msgctxt "@title:tab"
msgid "Checks"
msgstr "Έλεγχοι"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: config/clangtidyprojectconfigpage.ui:73
#, fuzzy, kde-format
#| msgid "Includes"
msgctxt "@title:tab"
msgid "Includes"
msgstr "Includes"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: config/clangtidyprojectconfigpage.ui:79
#, fuzzy, kde-format
#| msgid "&Check system headers:"
msgctxt "@option:check"
msgid "&Check system headers:"
msgstr "Έ&λεγχος κεφαλίδων συστήματος:"

#. i18n: ectx: property (text), widget (QLabel, headerFilterLabel)
#: config/clangtidyprojectconfigpage.ui:92
#, fuzzy, kde-format
#| msgid "Header fi&lter:"
msgctxt "@label:textbox"
msgid "Header fi&lter:"
msgstr "Φίλτρο κεφα&λίδας:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, kcfg_headerFilter)
#: config/clangtidyprojectconfigpage.ui:105
#, fuzzy, kde-format
#| msgid ""
#| "<html><head/><body><p>Regular expression matching the names of the "
#| "headers to output diagnostics from. Diagnostics from the main file of "
#| "each translation unit are always displayed. Can be used together with -"
#| "line-filter.</p></body></html>"
msgctxt "@info:tooltip"
msgid ""
"Regular expression matching the names of the headers to output diagnostics "
"from. Diagnostics from the main file of each translation unit are always "
"displayed. Can be used together with -line-filter."
msgstr ""
"<html><head/><body><p>Κανονικές εκφράσεις που ταιριάζουν με τα ονόματα των "
"κεφαλίδων για την εξαγωγή διαγνωστικών. Διαγνωστικά από το κύριο αρχείο κάθε "
"μεταφραστικής μονάδας εμφανίζονται πάντα. Μπορεί να χρησιμοποιηθεί μαζί με "
"το -line-filter.</p></body></html>"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: config/clangtidyprojectconfigpage.ui:119
#, fuzzy, kde-format
#| msgid "Extra Parameters"
msgctxt "@title:tab"
msgid "Extra Parameters"
msgstr "Επιπλέον παράμετροι"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: config/clangtidyprojectconfigpage.ui:125
#, fuzzy, kde-format
#| msgid "E&xtra parameters:"
msgctxt "@label.textbox"
msgid "E&xtra parameters:"
msgstr "Ε&πιπλέον παράμετροι:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, kcfg_additionalParameters)
#: config/clangtidyprojectconfigpage.ui:135
#, fuzzy, kde-format
#| msgid ""
#| "<html><head/><body><p>Additional command line options to pass to clang-"
#| "tidy.</p></body></html>"
msgctxt "@info:tooltip"
msgid "Additional command line options to pass to Clang-Tidy."
msgstr ""
"<html><head/><body><p>Πρόσθετες επιλογές γραμμής εντολών για το clang-tidy.</"
"p></body></html>"

#: job.cpp:68
#, kde-format
msgid "Clang-Tidy Analysis"
msgstr "Ανάλυση Clang-Tidy"

#: job.cpp:142
#, kde-format
msgid "Failed to start Clang-Tidy process."
msgstr "Αποτυχία έναρξης διεργασίας Clang-Tidy."

#: job.cpp:147
#, fuzzy, kde-format
#| msgid "Clang-tidy crashed."
msgid "Clang-Tidy crashed."
msgstr "Το Clang-Tidy κατέρρευσε."

#: job.cpp:151
#, fuzzy, kde-format
#| msgid "Clang-tidy process timed out."
msgid "Clang-Tidy process timed out."
msgstr "Ο χρόνος της διεργασίας clang-tidy εξέπνευσε."

#: job.cpp:155
#, fuzzy, kde-format
#| msgid "Write to Clang-tidy process failed."
msgid "Write to Clang-Tidy process failed."
msgstr "Αποτυχία εγγραφής στη διεργασία clang-tidy."

#: job.cpp:159
#, fuzzy, kde-format
#| msgid "Read from Clang-tidy process failed."
msgid "Read from Clang-Tidy process failed."
msgstr "Αποτυχία ανάγνωσης από τη διεργασία clang-tidy."

#~ msgid "Clang-tidy Error"
#~ msgstr "Σφάλμα clang-tidy"

#~ msgid "Analyze Current File with Clang-Tidy"
#~ msgstr "Ανάλυση του τρέχοντος αρχείου με το clang-tidy"

#~ msgid "Analyze Current Project with Clang-Tidy"
#~ msgstr "Ανάλυση τρέχοντος έργου με το clang-tidy"

#~ msgid "Error starting clang-tidy"
#~ msgstr "Σφάλμα στην εκκίνηση του clang-tidy"

#~ msgid "No suitable active file, unable to deduce project."
#~ msgstr "Δεν υπάρχει κατάλληλο ενεργό αρχείο, αδυναμία εξαγωγής έργου."

#~ msgid "Active file isn't in a project"
#~ msgstr "Το ενεργό αρχείο δεν είναι μέσα σε έργο"

#~ msgctxt "@title:window"
#~ msgid "Test"
#~ msgstr "Δοκιμή"

#~ msgid "Analysis started..."
#~ msgstr "Η ανάλυση ξεκίνησε..."

#~ msgctxt "@info:tooltip %1 is the path of the file"
#~ msgid "Re-run last Clang-Tidy analysis (%1)"
#~ msgstr "Επανάληψη εκτέλεσης ανάλυσης clang-tidy (%1)"

#~ msgctxt "@info:tooltip"
#~ msgid "Re-run last Clang-Tidy analysis"
#~ msgstr "Επανάληψη εκτέλεσης ανάλυσης clang-tidy"

#~ msgid "Analysis completed, no problems detected."
#~ msgstr "Η ανάλυση ολοκληρώθηκε, δεν ανιχνεύθηκαν προβλήματα."

#~ msgid "Compilation database file not found: '%1'"
#~ msgstr "Το αρχείο βάσης δεδομένων της μεταγλώττισης δεν βρέθηκε: '%1'"

#~ msgid "Could not open compilation database file for reading: '%1'"
#~ msgstr ""
#~ "Αδυναμία ανοίγματος του αρχείου βάσης δεδομένων της μεταγλώττισης για "
#~ "ανάγνωση: '%1'"

#~ msgid "JSON error during parsing compilation database file '%1': %2"
#~ msgstr ""
#~ "Σφάλμα JSON κατά την ανάλυση του αρχείου βάσης δεδομένων της "
#~ "μεταγλώττισης '%1': %2"

#~ msgid ""
#~ "JSON error during parsing compilation database file '%1': document is not "
#~ "an array."
#~ msgstr ""
#~ "Σφάλμα JSON κατά την ανάλυση του αρχείου βάσης δεδομένων της "
#~ "μεταγλώττισης '%1': το έγγραφο δεν είναι διάνυσμα."

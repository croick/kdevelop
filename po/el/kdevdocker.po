# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdevelop package.
#
# Stelios <sstavra@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdevelop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2019-08-27 13:20+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: dockerplugin.cpp:117
#, kde-format
msgid "docker build '%1'"
msgstr "docker κατασκευή '%1'"

#: dockerplugin.cpp:121
#, fuzzy, kde-format
#| msgid "Choose tag name..."
msgctxt "@title:window"
msgid "Choose Tag Name"
msgstr "Επιλογή ονόματος ετικέτας..."

#: dockerplugin.cpp:122
#, fuzzy, kde-format
#| msgid "Tag name for '%1'"
msgctxt "@label:textbox"
msgid "Tag name for '%1':"
msgstr "Όνομα ετικέτας για το '%1'"

#. i18n: ectx: property (text), widget (QLabel, label)
#: dockerpreferences.ui:29
#, fuzzy, kde-format
#| msgid "'docker run' arguments:"
msgctxt "@label:textbox"
msgid "'docker run' arguments:"
msgstr "'docker run' ορίσματα:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: dockerpreferences.ui:39
#, fuzzy, kde-format
#| msgid "Projects volume:"
msgctxt "@label:textbox"
msgid "Projects volume:"
msgstr "Όγκος έργων:"

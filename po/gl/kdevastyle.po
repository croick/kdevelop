# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Adrian Chaves <adrian@chaves.io>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-05 00:44+0000\n"
"PO-Revision-Date: 2019-07-20 18:56+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: astyle_plugin.cpp:227
#, kde-format
msgid ""
"<b>Artistic Style</b> is a source code indenter, formatter, and beautifier "
"for the C, C++, C# and Java programming languages.<br />Home Page: <a href="
"\"http://astyle.sourceforge.net/\">http://astyle.sourceforge.net</a>"
msgstr ""
"<b>Estilo artístico</b> é un formatador de código fonte para as linguaxes de "
"programación C, C++, C# e Java.<br />Páxina do proxecto: <a href=\"http://"
"astyle.sourceforge.net/\">http://astyle.sourceforge.net</a>"

#. i18n: ectx: attribute (title), widget (QWidget, tabIndentqtion)
#: astyle_preferences.ui:33
#, fuzzy, kde-format
#| msgid "Indentation"
msgctxt "@title:tab"
msgid "Indentation"
msgstr "Sangrado"

#. i18n: ectx: property (text), item, widget (KComboBox, cbIndentType)
#: astyle_preferences.ui:48
#, fuzzy, kde-format
#| msgctxt "Indentation type"
#| msgid "Tabs"
msgctxt "@item:inlistbox Indentation type"
msgid "Tabs"
msgstr "Tabulacións"

#. i18n: ectx: property (text), item, widget (KComboBox, cbIndentType)
#: astyle_preferences.ui:53
#, fuzzy, kde-format
#| msgctxt "Indentation type"
#| msgid "Force tabs"
msgctxt "@item:inlistbox Indentation type"
msgid "Force tabs"
msgstr "Forzar as tabulacións"

#. i18n: ectx: property (text), item, widget (KComboBox, cbIndentType)
#: astyle_preferences.ui:58
#, fuzzy, kde-format
#| msgctxt "Indentation type"
#| msgid "Spaces"
msgctxt "@item:inlistbox Indentation type"
msgid "Spaces"
msgstr "Espazos"

#. i18n: ectx: property (toolTip), widget (QSpinBox, inpNuberSpaces)
#: astyle_preferences.ui:86
#, fuzzy, kde-format
#| msgid ""
#| "Number of spaces that will be converted to a tab.\n"
#| "The number of spaces per tab is controlled by the editor."
msgctxt "@info:tooltip"
msgid ""
"Number of spaces that will be converted to a tab.\n"
"The number of spaces per tab is controlled by the editor."
msgstr ""
"Cantos espazos se converterán nun tabulador.\n"
"A cantidade de espazos por tabulador é controlada polo editor."

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkConvertTabs)
#: astyle_preferences.ui:104
#, fuzzy, kde-format
#| msgid "Convert tabs to spaces."
msgctxt "@info:tooltip"
msgid "Convert tabs to spaces."
msgstr "Converte os tabuladores en espazos."

#. i18n: ectx: property (text), widget (QCheckBox, chkConvertTabs)
#: astyle_preferences.ui:107
#, fuzzy, kde-format
#| msgid "Con&vert tabs into spaces"
msgctxt "@option:check"
msgid "Con&vert tabs into spaces"
msgstr "Con&verter os tabuladores es espazos"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkFillEmptyLines)
#: astyle_preferences.ui:114
#, fuzzy, kde-format
#| msgid "Fill empty lines with the white space of their previous lines."
msgctxt "@info:tooltip"
msgid "Fill empty lines with the white space of their previous lines."
msgstr "Encher as liñas baleiras cos espazos da liña anterior."

#. i18n: ectx: property (text), widget (QCheckBox, chkFillEmptyLines)
#: astyle_preferences.ui:117
#, fuzzy, kde-format
#| msgid "Fill empt&y lines"
msgctxt "@option:check"
msgid "Fill empt&y lines"
msgstr "&Encher as liñas baleiras"

#. i18n: ectx: property (text), widget (QLabel, lblIndentObjects)
#: astyle_preferences.ui:139
#, fuzzy, kde-format
#| msgid "Indent:"
msgctxt "@label:listbox"
msgid "Indent:"
msgstr "Sangrado:"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:153
#, fuzzy, kde-format
#| msgid "Blocks"
msgctxt "@item:inlistbox"
msgid "Blocks"
msgstr "Bloques"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:158
#, fuzzy, kde-format
#| msgid "Brackets"
msgctxt "@item:inlistbox"
msgid "Brackets"
msgstr "Chaves"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:163
#, fuzzy, kde-format
#| msgid "Cases"
msgctxt "@item:inlistbox"
msgid "Cases"
msgstr "Casos"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:168
#, fuzzy, kde-format
#| msgid "Class"
msgctxt "@item:inlistbox"
msgid "Class"
msgstr "Clase"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:173
#, fuzzy, kde-format
#| msgid "Labels"
msgctxt "@item:inlistbox"
msgid "Labels"
msgstr "Etiquetas"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:178
#, fuzzy, kde-format
#| msgid "Namespaces"
msgctxt "@item:inlistbox"
msgid "Namespaces"
msgstr "Espazos de nomes"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:183
#, fuzzy, kde-format
#| msgid "Preprocessor directives"
msgctxt "@item:inlistbox"
msgid "Preprocessor directives"
msgstr "Directivas do preprocesador"

#. i18n: ectx: property (text), item, widget (QListWidget, listIdentObjects)
#: astyle_preferences.ui:188
#, fuzzy, kde-format
#| msgid "Switches"
msgctxt "@item:inlistbox"
msgid "Switches"
msgstr "Conmutadores"

#. i18n: ectx: property (toolTip), widget (QSpinBox, inpMaxStatement)
#. i18n: ectx: property (toolTip), widget (QLabel, TextLabel2_2)
#: astyle_preferences.ui:201 astyle_preferences.ui:243
#, fuzzy, kde-format
#| msgid ""
#| "Indent a maximal # spaces in a continuous statement,\n"
#| "relative to the previous line."
msgctxt "@info:tooltip"
msgid ""
"Indent a maximal # spaces in a continuous statement,\n"
"relative to the previous line."
msgstr ""
"Sangrar unha cantidade máxima de espazos nunha sentenza continua\n"
"en relación á liña anterior."

#. i18n: ectx: property (toolTip), widget (QLabel, TextLabel3_2)
#. i18n: ectx: property (toolTip), widget (QSpinBox, inpMinConditional)
#: astyle_preferences.ui:212 astyle_preferences.ui:226
#, fuzzy, kde-format
#| msgid ""
#| "Indent a minimal # spaces in a continuous conditional\n"
#| "belonging to a conditional header."
msgctxt "@info:tooltip"
msgid ""
"Indent a minimal # spaces in a continuous conditional\n"
"belonging to a conditional header."
msgstr ""
"Sangrar unha cantidade mínima de espazos nunha condición continua\n"
"que pertenza a unha cabeceira condicional."

#. i18n: ectx: property (text), widget (QLabel, TextLabel3_2)
#: astyle_preferences.ui:215
#, fuzzy, kde-format
#| msgid "Minimum in conditional:"
msgctxt "@label:spinbox"
msgid "Minimum in conditional:"
msgstr "Mínimo no condicional:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, inpMinConditional)
#: astyle_preferences.ui:229
#, fuzzy, kde-format
#| msgid "Twice current"
msgctxt "@item"
msgid "Twice current"
msgstr "O duplo do actual"

#. i18n: ectx: property (text), widget (QLabel, TextLabel2_2)
#: astyle_preferences.ui:246
#, fuzzy, kde-format
#| msgid "Maximum in statement:"
msgctxt "@label:spinbox"
msgid "Maximum in statement:"
msgstr "Máximo na sentenza:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, inpContinuation)
#: astyle_preferences.ui:256
#, fuzzy, kde-format
#| msgid "Number of indents"
msgctxt "@info:tooltip"
msgid "Number of indents"
msgstr "Número de niveis de sangrado"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkAfterParens)
#: astyle_preferences.ui:269
#, fuzzy, kde-format
#| msgid ""
#| "Indent, instead of align, continuation lines following lines that contain "
#| "an opening paren '(' or an assignment '='."
msgctxt "@info:tooltip"
msgid ""
"Indent, instead of align, continuation lines following lines that contain an "
"opening paren '(' or an assignment '='."
msgstr ""
"Sangrar as liñas de continuación en vez de aliñar ao seguir a liñas que "
"conteñen un parénteses inicial, «(», ou unha asignación, «=»."

#. i18n: ectx: property (text), widget (QCheckBox, chkAfterParens)
#: astyle_preferences.ui:272
#, fuzzy, kde-format
#| msgid "Continuation lines:"
msgctxt "@option:check"
msgid "Continuation lines:"
msgstr "Liñas de continuación:"

#. i18n: ectx: attribute (title), widget (QWidget, tabOther)
#: astyle_preferences.ui:295
#, fuzzy, kde-format
#| msgid "Other"
msgctxt "@title:tab"
msgid "Other"
msgstr "Outro"

#. i18n: ectx: property (title), widget (QGroupBox, gpBrackets)
#: astyle_preferences.ui:301
#, fuzzy, kde-format
#| msgid "Brackets"
msgctxt "@title:group"
msgid "Brackets"
msgstr "Chaves"

#. i18n: ectx: property (text), widget (QLabel, lblBrackets)
#: astyle_preferences.ui:315
#, fuzzy, kde-format
#| msgid "Type:"
msgctxt "@label:listbox"
msgid "Type:"
msgstr "Tipo:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkBracketsCloseHeaders)
#: astyle_preferences.ui:326
#, fuzzy, kde-format
#| msgid ""
#| "Break brackets before closing headers (e.g. 'else', 'catch', ...)\n"
#| "from their immediately preceding closing brackets."
msgctxt "@info:tooltip"
msgid ""
"Break brackets before closing headers (e.g. 'else', 'catch', ...)\n"
"from their immediately preceding closing brackets."
msgstr ""
"Racha os corchetes antes de pechar as cabeceiras (por ex. «else», «catch» "
"…)\n"
"do corchete de peche que o precede."

#. i18n: ectx: property (text), widget (QCheckBox, chkBracketsCloseHeaders)
#: astyle_preferences.ui:329
#, fuzzy, kde-format
#| msgid "Brea&k closing headers"
msgctxt "@option:check"
msgid "Brea&k closing headers"
msgstr "Rachar os &peches das cabeceiras"

#. i18n: ectx: property (text), item, widget (KComboBox, cbBrackets)
#: astyle_preferences.ui:343
#, fuzzy, kde-format
#| msgid "No change"
msgctxt "@item:inlistbox brackets style"
msgid "No change"
msgstr "Sen cambios"

#. i18n: ectx: property (text), item, widget (KComboBox, cbBrackets)
#: astyle_preferences.ui:348
#, fuzzy, kde-format
#| msgid "Attach"
msgctxt "@item:inlistbox brackets style"
msgid "Attach"
msgstr "Anexar"

#. i18n: ectx: property (text), item, widget (KComboBox, cbBrackets)
#: astyle_preferences.ui:353
#, fuzzy, kde-format
#| msgid "Break"
msgctxt "@item:inlistbox brackets style"
msgid "Break"
msgstr "Parada"

#. i18n: ectx: property (text), item, widget (KComboBox, cbBrackets)
#: astyle_preferences.ui:358
#, fuzzy, kde-format
#| msgid "Linux"
msgctxt "@item:inlistbox brackets style"
msgid "Linux"
msgstr "Linux"

#. i18n: ectx: property (text), item, widget (KComboBox, cbBrackets)
#: astyle_preferences.ui:363
#, fuzzy, kde-format
#| msgid "Run-In"
msgctxt "@item:inlistbox brackets style"
msgid "Run-In"
msgstr "Run-In"

#. i18n: ectx: property (title), widget (QGroupBox, gpBlocks)
#: astyle_preferences.ui:376
#, fuzzy, kde-format
#| msgid "Blocks"
msgctxt "@title:group"
msgid "Blocks"
msgstr "Bloques"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkBlockBreak)
#: astyle_preferences.ui:391
#, fuzzy, kde-format
#| msgid ""
#| "Insert empty lines around unrelated blocks, labels, classes,...\n"
#| "Known problems:\n"
#| "\n"
#| "1. If a statement is NOT part of a block, \n"
#| "the following statements are all double spaced. \n"
#| "Statements enclosed in a block are formatted \n"
#| "correctly.\n"
#| "\n"
#| "2. Comments are broken from the block.\n"
msgctxt "@info:tooltip"
msgid ""
"Insert empty lines around unrelated blocks, labels, classes,...\n"
"Known problems:\n"
"\n"
"1. If a statement is NOT part of a block, \n"
"the following statements are all double spaced. \n"
"Statements enclosed in a block are formatted \n"
"correctly.\n"
"\n"
"2. Comments are broken from the block.\n"
msgstr ""
"Insire liñas baleiras arredor de bloques, etiquetas, clases etc, non "
"relacionados\n"
"Problemas coñecidos:\n"
"\n"
"1.- Se unha sentenza NON forma parte dun bloque,\n"
"as seguintes sentenzas terán todas espaciado duplo.\n"
"As sentenzas envoltas nun bloque formataranse correctamente.\n"
"2. Os comentarios separaranse do bloque.\n"

#. i18n: ectx: property (text), widget (QCheckBox, chkBlockBreak)
#: astyle_preferences.ui:394
#, fuzzy, kde-format
#| msgctxt "Means break unrelated blocks by a newline"
#| msgid "&Break blocks"
msgctxt "@option:check break unrelated blocks by a newline"
msgid "&Break blocks"
msgstr "&Separar os bloques"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkBlockBreakAll)
#: astyle_preferences.ui:412
#, fuzzy, kde-format
#| msgid ""
#| "Like --break-blocks, except also insert empty lines \n"
#| "around closing headers (e.g. 'else', 'catch', ...).\n"
#| "\n"
#| "Known problems:\n"
#| "\n"
#| "1. If a statement is NOT part of a block, \n"
#| "the following statements are all double spaced. \n"
#| "Statements enclosed in a block are formatted \n"
#| "correctly.\n"
#| "\n"
#| "2. Comments are broken from the block.\n"
msgctxt "@info:tooltip"
msgid ""
"Like --break-blocks, except also insert empty lines \n"
"around closing headers (e.g. 'else', 'catch', ...).\n"
"\n"
"Known problems:\n"
"\n"
"1. If a statement is NOT part of a block, \n"
"the following statements are all double spaced. \n"
"Statements enclosed in a block are formatted \n"
"correctly.\n"
"\n"
"2. Comments are broken from the block.\n"
msgstr ""
"Como --break-blocks, agás que tamén insire liñas baleiras\n"
"arredor das cabeceiras de peche (por ex. «else», «catch» …).\n"
"\n"
"Problemas coñecidos:\n"
"\n"
"1. Se unha sentenza NON forma parte dun bloque,\n"
"as sentenzas que a sigan estarán todas con espazo duplo.\n"
"As pechadas nun bloque han ser formatadas correctamente.\n"
"\n"
"2. Os comentarios son separados do bloque.\n"

#. i18n: ectx: property (text), widget (QCheckBox, chkBlockBreakAll)
#: astyle_preferences.ui:415
#, fuzzy, kde-format
#| msgctxt "Means break all blocks with a newline"
#| msgid "Break all bl&ocks"
msgctxt "@option:check break all blocks with a newline"
msgid "Break all bl&ocks"
msgstr "Separar &todos os bloques"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkBlockIfElse)
#: astyle_preferences.ui:422
#, fuzzy, kde-format
#| msgid "Break 'else if()' statements into two different lines."
msgctxt "@info:tooltip"
msgid "Break 'else if()' statements into two different lines."
msgstr "Separa as sentenzas «else if()» en dúas liñas diferentes."

#. i18n: ectx: property (text), widget (QCheckBox, chkBlockIfElse)
#: astyle_preferences.ui:425
#, fuzzy, kde-format
#| msgctxt "Means break else if() into separate lines"
#| msgid "Break i&f-else"
msgctxt "@option:check break else if() into separate lines"
msgid "Break i&f-else"
msgstr "Separar &if-else"

#. i18n: ectx: property (title), widget (QGroupBox, gpPadding)
#: astyle_preferences.ui:435
#, fuzzy, kde-format
#| msgid "Padding"
msgctxt "@title:group"
msgid "Padding"
msgstr "Separación de palabras"

#. i18n: ectx: property (text), item, widget (KComboBox, cbParenthesisPadding)
#: astyle_preferences.ui:450
#, fuzzy, kde-format
#| msgid "No change"
msgctxt "@item:inlistbox parenthesis padding"
msgid "No change"
msgstr "Sen cambios"

#. i18n: ectx: property (text), item, widget (KComboBox, cbParenthesisPadding)
#: astyle_preferences.ui:455
#, fuzzy, kde-format
#| msgid "Unpad"
msgctxt "@item:inlistbox parenthesis padding"
msgid "Unpad"
msgstr "Anulación da separación"

#. i18n: ectx: property (text), item, widget (KComboBox, cbParenthesisPadding)
#: astyle_preferences.ui:460
#, fuzzy, kde-format
#| msgid "Inside only"
msgctxt "@item:inlistbox parenthesis padding"
msgid "Inside only"
msgstr "Só dentro"

#. i18n: ectx: property (text), item, widget (KComboBox, cbParenthesisPadding)
#: astyle_preferences.ui:465
#, fuzzy, kde-format
#| msgid "Outside only"
msgctxt "@item:inlistbox parenthesis padding"
msgid "Outside only"
msgstr "Só fora"

#. i18n: ectx: property (text), item, widget (KComboBox, cbParenthesisPadding)
#: astyle_preferences.ui:470
#, fuzzy, kde-format
#| msgid "Inside and outside"
msgctxt "@item:inlistbox parenthesis padding"
msgid "Inside and outside"
msgstr "Dentro e fora"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkPadOperators)
#: astyle_preferences.ui:480
#, fuzzy, kde-format
#| msgid ""
#| "Insert space padding around operators.\n"
#| "Once padded, operators stay padded.\n"
#| "There is no unpad operator option."
msgctxt "@info:tooltip"
msgid ""
"Insert space padding around operators.\n"
"Once padded, operators stay padded.\n"
"There is no unpad operator option."
msgstr ""
"Insire recheo de espazos arredor dos operadores.\n"
"Unha vez enchidos, os operadores seguen enchidos.\n"
"Non hai ningunha opción para retirar o recheo."

#. i18n: ectx: property (text), widget (QCheckBox, chkPadOperators)
#: astyle_preferences.ui:483
#, fuzzy, kde-format
#| msgid "&Add spaces around operators"
msgctxt "@option:check"
msgid "&Add spaces around operators"
msgstr "Engadir espazos arredor dos &operadores"

#. i18n: ectx: property (text), widget (QLabel, lblParenthesis)
#: astyle_preferences.ui:490
#, fuzzy, kde-format
#| msgid "Pad parenthesis:"
msgctxt "@label:listbox"
msgid "Pad parenthesis:"
msgstr "Separar os parénteses:"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkPadParenthesisHeader)
#: astyle_preferences.ui:497
#, fuzzy, kde-format
#| msgid ""
#| "Insert space padding after parenthesis headers (e.g. 'if', 'for', "
#| "'while', ...)"
msgctxt "@info:tooltip"
msgid ""
"Insert space padding after parenthesis headers (e.g. 'if', 'for', "
"'while', ...)"
msgstr ""
"Inserir espazo de recheo despois dos cabezallos dos parénteses (p.ex. «if», "
"«for», «while»,…)"

#. i18n: ectx: property (text), widget (QCheckBox, chkPadParenthesisHeader)
#: astyle_preferences.ui:500
#, fuzzy, kde-format
#| msgid "Add spaces after parenthesis &headers"
msgctxt "@option:check"
msgid "Add spaces after parenthesis &headers"
msgstr "Engadir espazos despois dos &cabezallos dos parénteses"

#. i18n: ectx: property (title), widget (QGroupBox, gpOneLiners)
#: astyle_preferences.ui:512
#, fuzzy, kde-format
#| msgid "One liners"
msgctxt "@title:group"
msgid "One Liners"
msgstr "Liñas únicas"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkKeepStatements)
#: astyle_preferences.ui:519
#, fuzzy, kde-format
#| msgid ""
#| "Do not break lines containing multiple statements into\n"
#| "multiple single-statement lines."
msgctxt "@info:tooltip"
msgid ""
"Do not break lines containing multiple statements into\n"
"multiple single-statement lines."
msgstr ""
"Non separa as liñas que conteñen varias sentenzas en varias\n"
"liñas contendo unha só sentenza cada unha."

#. i18n: ectx: property (text), widget (QCheckBox, chkKeepStatements)
#: astyle_preferences.ui:522
#, fuzzy, kde-format
#| msgid "&Keep one-line statements"
msgctxt "@option:check"
msgid "&Keep one-line statements"
msgstr "Manter as sentenzas dunha li&ña"

#. i18n: ectx: property (toolTip), widget (QCheckBox, chkKeepBlocks)
#: astyle_preferences.ui:529
#, fuzzy, kde-format
#| msgid "Do not break blocks residing completely on one line."
msgctxt "@info:tooltip"
msgid "Do not break blocks residing completely on one line."
msgstr "Non racha os bloques que só ocupen unha liña."

#. i18n: ectx: property (text), widget (QCheckBox, chkKeepBlocks)
#: astyle_preferences.ui:532
#, fuzzy, kde-format
#| msgid "Keep o&ne-line blocks"
msgctxt "@option:check"
msgid "Keep o&ne-line blocks"
msgstr "Manter os &bloques dunha liña"

#. i18n: ectx: property (title), widget (QGroupBox, gpOther)
#: astyle_preferences.ui:542
#, fuzzy, kde-format
#| msgid "Other"
msgctxt "@title:group"
msgid "Other"
msgstr "Outro"

#. i18n: ectx: property (text), widget (QLabel, lblPointerAlign)
#: astyle_preferences.ui:548
#, fuzzy, kde-format
#| msgid "Pointer Alignment:"
msgctxt "@label:listbox"
msgid "Pointer Alignment:"
msgstr "Aliñamento do punteiro:"

#. i18n: ectx: property (text), item, widget (QComboBox, cbPointerAlign)
#: astyle_preferences.ui:556
#, fuzzy, kde-format
#| msgid "No change"
msgctxt "@item:inlistbox pointer alignment"
msgid "No change"
msgstr "Sen cambios"

#. i18n: ectx: property (text), item, widget (QComboBox, cbPointerAlign)
#: astyle_preferences.ui:561
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@item:inlistbox pointer alignment"
msgid "Name"
msgstr "Nome"

#. i18n: ectx: property (text), item, widget (QComboBox, cbPointerAlign)
#: astyle_preferences.ui:566
#, fuzzy, kde-format
#| msgid "Middle"
msgctxt "@item:inlistbox pointer alignment"
msgid "Middle"
msgstr "Medio"

#. i18n: ectx: property (text), item, widget (QComboBox, cbPointerAlign)
#: astyle_preferences.ui:571
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@item:inlistbox pointer alignment"
msgid "Type"
msgstr "Tipo"

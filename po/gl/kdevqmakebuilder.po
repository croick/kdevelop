# translation of kdevqmakebuilder.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kdevqmakebuilder\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-02 00:46+0000\n"
"PO-Revision-Date: 2015-02-21 12:06+0100\n"
"Last-Translator: Adrián Chaves Fernández <adriyetichaves@gmail.com>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: qmakebuilderpreferences.cpp:81
#, fuzzy, kde-format
#| msgid "QMake"
msgctxt "@title:tab"
msgid "QMake"
msgstr "QMake"

#: qmakebuilderpreferences.cpp:140
#, fuzzy, kde-format
#| msgid ""
#| "The %1 directory is about to be removed in KDevelop's list.\n"
#| "Do you want KDevelop to remove it in the file system as well?"
msgid ""
"The %1 directory is about to be removed in KDevelop's list.\n"
"Do you want KDevelop to delete it in the file system as well?"
msgstr ""
"Estase a piques de retirar o directorio %1 da lista de KDevelop.\n"
"Quere que KDevelop o elimine tamén do sistema de ficheiros?"

#: qmakebuilderpreferences.cpp:144
#, kde-format
msgctxt "@action:button"
msgid "Do Not Delete"
msgstr ""

#: qmakebuilderpreferences.cpp:149
#, kde-format
msgid "Could not remove: %1."
msgstr "Non foi posíbel eliminar «%1»."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: qmakeconfig.ui:77
#, fuzzy, kde-format
#| msgid "Build Settings"
msgctxt "@title:group"
msgid "Build Settings"
msgstr "Configuración da construción"

#: qmakejob.cpp:32
#, fuzzy, kde-format
#| msgid "QMake"
msgctxt "@title:window"
msgid "QMake"
msgstr "QMake"

#: qmakejob.cpp:43
#, kde-format
msgid "No project specified."
msgstr "Non especificou nengún proxecto."

#: qmakejob.cpp:82
#, kde-format
msgid "QMake: %1"
msgstr "QMake: %1"

#: qmakejob.cpp:92
#, kde-format
msgid "Configure error"
msgstr "Erro en configure"

#~ msgid "Add"
#~ msgstr "Engadir"

#~ msgid "Remove"
#~ msgstr "Retirar"

#~ msgid "Current changes will be lost. Would you want to save them?"
#~ msgstr "Perderanse os cambios actuais. Quere gardalos?"

#~ msgid "QMake Builder"
#~ msgstr "Construtor QMake"

#~ msgid "Support for building QMake projects"
#~ msgstr "Soporte de construción de proxectos con QMake"
